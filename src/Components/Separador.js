import React, { Component } from 'react'

import "../Css/Separador.css";

export default class Separador extends Component {
    render() {
        return (
            <>
              <hr className="Separador SeparadorArriba" />
              <hr className="Separador SeparadorAbajo" />  
            </>
        )
    }
}
