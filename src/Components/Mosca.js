import React, { Component } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faDragon } from '@fortawesome/free-solid-svg-icons'

import "../Css/Mosca.css";

export default class Mosca extends Component {
    render() {
        return (
            <FontAwesomeIcon icon={faDragon} className="Mosca" style={{ color: this.props.color }} />
        )
    }
}
