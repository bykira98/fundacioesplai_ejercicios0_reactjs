import React, { Component } from 'react'

import "../Css/Gato.css";
import { runInThisContext } from 'vm';

export default class Gato extends Component {
    render() {
        let link = `http://placekitten.com/${this.props.ancho}/${this.props.alto}`;
        let AnchoCaja = parseInt(this.props.ancho) + 10;
        let AltoCaja = parseInt(this.props.alto) + 50;
        return (
            <div className="Gato_Box" style={{ width: AnchoCaja, height: AltoCaja }}>
                <img className="Gato_Imagen" src={link} alt={this.props.nombre} />
                <p className="Gato_Nombre">{this.props.nombre}</p>
            </div>
        )
    }
}
