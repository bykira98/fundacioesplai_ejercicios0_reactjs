import React, { Component } from 'react'

import "../css/Bola.css";
import "../css/BolaBingo.css";

export default class BolaBingo extends Component {
    render() {
        return (
            <div className="Bola">
                <p className="NumeroBola">{this.props.num}</p>
            </div>
        )
    }
}
