import React, { Component } from 'react'

import "../Css/FotoBola.css";

export default class FotoBola extends Component {
    render() {
        return (
            <img src={this.props.src} style={{ width: this.props.talla, height: this.props.talla, borderRadius: this.props.talla / 2 }} />
        )
    }
}
