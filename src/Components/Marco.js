import React, { Component } from 'react'

import "../Css/Marco.css";

export default class Marco extends Component {
    render() {
        return (
            <div style={{ borderWidth: "3px", borderColor: this.props.color, borderStyle: "solid", padding: this.props.borde, backgroundColor: this.props.fondo, display: "inline-block" }}>
                <img src={this.props.src} />
            </div>
        )
    }
}
