import React, { Component } from 'react'

import "../Css/Titulo.css";

export default class Titulo extends Component {
    render() {
        return (
            <h1>{this.props.texto}</h1>
        )
    }
}
