import React from "react";

import HolaMundo from "./Components/HolaMundo";
import Bola from "./Components/Bola";
import Cuadrado from "./Components/Cuadrado";
import Separador from "./Components/Separador";
import Titulo from "./Components/Titulo";
import BolaX from "./Components/BolaX";
import CuadradoB from "./Components/CuadradoB";
import Mosca from "./Components/Mosca";
import Capital from "./Components/Capital";
import Gato from "./Components/Gato";
import BolaBingo from "./Components/BolaBingo";
import FotoBola from "./Components/FotoBola";
import Marco from "./Components/Marco";
import Flipicon from "./Components/Flipicon";

export default () => (
  <>
    <HolaMundo />
    <Bola />
    <Cuadrado />
    <Separador />
    <Titulo texto="Hola React!" />
    <BolaX talla="50" margen="10" color="#ff0000" />
    <CuadradoB talla="70" margen="8" grosor="10" color="red" />
    <Mosca color="blue" />
    <Capital nom="Barcelona" />
    <Gato ancho="200" alto="301" nombre="Garfield" />
    <BolaBingo num="22" />
    <FotoBola src="http://lorempixel.com/200/200" talla="200" />
    <Marco src="http://lorempixel.com/200/300" borde="10" color="brown" fondo="beige" />
    <Flipicon icon1="fa-thumbs-up" icon2="fa-thumbs-down" />
  </>
);
